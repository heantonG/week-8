# 第一步：
# 生成一个正弦波的
# 有4个周期以上
# A = 100， Omega=16，Psei=0 
# 请生成Noise，强度是20.。
# 显示画出生成的结果

# 第二步： 去噪
#   5滑动的平滑
#   -2, -1, 0 , 1 2 
#   10滑动的平滑
# 画图

# 第三步：
# 搜索所有的极值

from cProfile import label
import numpy as np
import matplotlib.pyplot as plt 
import random

#寻峰函数
def peak(data_2d):
    centroids = []
    variable1 = data_2d[:,0]
    values = data_2d[:,1]
    values1 = abs(data_2d[:,1])
    threahhold = 40
    x_peaks1 = []

    for i in range(len(values1)-1):
        if (values1[i] > values1[i+1]) and (values1[i] > values1[i-1]) and (values1[i] > threahhold):
            if (values1[i] > values1[i-2]) and (values1[i] > values1[i+2]):
                x_peaks1.append(i)
    
    for i in x_peaks1:
        half_max1 = values1[i]/2
        xmin1 = (np.where(values1[i::-1] <= half_max1)[0])[0] - 1
        xmax1 = (np.where(values1[i::1] <= half_max1)[0])[0]
        
        x_range1 = variable1[i-xmin1:i+xmax1]
        I_range1 = values[i-xmin1:i+xmax1]
        x_range1 = np.array(x_range1)
        I_range1 = np.array(I_range1)
        
        xcm = np.sum((x_range1*I_range1))/np.sum(I_range1)
        
        centroids.append(xcm)
    return centroids

x = np.linspace(0,360*4,2000)
y1 = 100*np.sin(4*np.pi*x/180.)
noise = np.random.randint(-20,20,2000)
y2 = y1 + noise

#五点平滑
y3 = np.array([[0,0]])
for i in range(2,len(y2)-2):
    s5 = (1/5)*(y2[i-2]+y2[i-1]+y2[i]+y2[i+1]+y2[i+2])
    y33 = np.array([[i,s5]])
    y3 = np.row_stack([y3,y33])
y3 = np.delete(y3, 0, 0)
#寻峰
peak_s5 = peak(y3)
print(peak_s5)

#十一点平滑
y4 = np.array([[0,0]])
for i in range(5,len(y2)-5):
    s11 = sum(y2[i-5:i+6])/11
    y44 = np.array([[i,s11]])
    y4 = np.row_stack([y4,y44])
y4 = np.delete(y4, 0, 0)
#寻峰
peak_s11 = peak(y4)
print(peak_s11)

#画图
fig, ax = plt.subplots(3, 1, figsize=(5,5))
ax[0].plot(x, y2, color='r', label="origin")
ax[0].legend()
ax[1].plot(y3[:,0], y3[:,1], color="g", label="5")
ax[1].legend()
ax[2].plot(y4[:,0], y4[:,1], color="b", label="11")
ax[2].legend()

plt.show()



    






